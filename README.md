﻿## Novibet Assessment Test - Readme ##

This project is the outcome of the Novibet test assessment, with the requirements written on C# Interview Project document.

### Tools ###

* Visual Studio 2019, Jetbrains Rider
* .net core 3.1
* SQL Server Express
* SQL Server Management Studio

### Set Up Database ###

* Create MSSQL Database in SQL Server with name novibetWebApi. If you would like to use another name you can do, but you will need to change for sure the setting inside the appsettings.json
* In the appsettings.json of the WebApi project is located the DB connection string, by default the name of the Database is novibetWebApi. If you need must change it to match with your database settings

### Build ###

* Clone solution local
* Make sure you have .Net Core v3.1.2 installed on your machine. https://dotnet.microsoft.com/download/dotnet-core/3.1
* Build the solution with the above IDE tools
* Run WebApi project from the solution
* For the proper function of the WebApi the NovibetAssessmentTest.IpInfoProvider.dll must be part of the project assemplies (In the specific git repo, it is by default)

### Application Details ###

* The application uses Entity Framework with Code First approach, so in the beggining of the application, all necessary tables will be auto created
* In appsettings.json file inside the WebApi project, there are some main settings, like the batch threshold and cache expiring time
* The project will expose the API documentation on the browser
* You can navigate to Job Dashboard by typing after the port /hangfire, for example https://localhost:5001/hangfire

### Who do I talk to? ###

* Manos Parcharidis
* mparcharidis@gmail.com