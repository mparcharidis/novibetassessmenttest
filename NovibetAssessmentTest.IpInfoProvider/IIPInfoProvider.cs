﻿using System.Threading.Tasks;
using NovibetAssessmentTest.IpInfoProvider.Models;

namespace NovibetAssessmentTest.IpInfoProvider
{
	public interface IIPInfoProvider
	{
		Task<IPDetails> GetDetails(string ip);
	}
}