﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NovibetAssessmentTest.IpInfoProvider.Exceptions;
using NovibetAssessmentTest.IpInfoProvider.Models;

namespace NovibetAssessmentTest.IpInfoProvider
{
	public class IPInfoProvider : IIPInfoProvider
	{
		public async Task<IPDetails> GetDetails(string ip)
		{
			var ipDetails = new IPDetails();

			try
			{
				using (var client = new HttpClient())
				{
					var response = await client.GetAsync($"http://api.ipstack.com/{ip}?access_key=72adcfa29f38aeadb8eacfd8a22f225a");
					string content = await response.Content.ReadAsStringAsync();
				
					ipDetails = JsonConvert.DeserializeObject<IPDetails>(content);
				}
			}
			catch (Exception ex)
			{
				throw new IPServiceNotAvailableException(ex.Message, ex.InnerException);
			}

			return ipDetails;
		}
	}
}