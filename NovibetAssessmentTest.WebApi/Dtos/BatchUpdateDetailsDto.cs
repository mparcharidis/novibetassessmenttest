﻿using System;
using System.ComponentModel.DataAnnotations;
using Heroic.AutoMapper;
using NovibetAssessmentTest.WebApi.Models;

namespace NovibetAssessmentTest.WebApi.Dtos
{
	public class BatchUpdateDetailsDto : IMapFrom<BatchUpdateDetails>
	{
		[Required]
		public Guid JobId { get; set; }
		
		[Required]
		public int TotalIps { get; set; }
		
		[Required]
		public int CurrentIpsCounter { get; set; }
	}
}