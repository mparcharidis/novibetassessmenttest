﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NovibetAssessmentTest.WebApi.Code.Bus;

namespace NovibetAssessmentTest.WebApi.Dtos.Commands
{
	public class IpDetailsBatchUpdateCommand : Command
	{
		[Required]
		public List<IpDetailsDto> IpDetailsDtoList { get; set; } 
	}
}