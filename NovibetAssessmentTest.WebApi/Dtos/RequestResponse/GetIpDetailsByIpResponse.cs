﻿namespace NovibetAssessmentTest.WebApi.Dtos.RequestResponse
{
	public class GetIpDetailsByIpResponse
	{
		public IpDetailsDto IpDetailsDto { get; }

		public GetIpDetailsByIpResponse(IpDetailsDto ipDetailsDto)
		{
			IpDetailsDto = ipDetailsDto;
		}
	}
}