﻿namespace NovibetAssessmentTest.WebApi.Dtos.RequestResponse
{
	public class GetJobDetailsByIpResponse
	{
		public BatchUpdateDetailsDto BatchUpdateDetailsDto { get; }

		public GetJobDetailsByIpResponse(BatchUpdateDetailsDto batchUpdateDetailsDto)
		{
			BatchUpdateDetailsDto = batchUpdateDetailsDto;
		}
	}
}