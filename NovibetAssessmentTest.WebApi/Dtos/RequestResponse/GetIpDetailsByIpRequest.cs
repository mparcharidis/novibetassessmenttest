﻿using System;
using System.ComponentModel.DataAnnotations;
using NovibetAssessmentTest.WebApi.Code.Validation;

namespace NovibetAssessmentTest.WebApi.Dtos.RequestResponse
{
	public class GetIpDetailsByIpRequest : ValidateMe
	{
		[Required] 
		public string Ip { get; set; }

		public GetIpDetailsByIpRequest(string ip)
		{
			Ip = ip;
		}
	}
}