﻿using System;
using System.ComponentModel.DataAnnotations;
using NovibetAssessmentTest.WebApi.Code.Validation;

namespace NovibetAssessmentTest.WebApi.Dtos.RequestResponse
{
	public class GetJobDetailsByIdRequest : ValidateMe
	{
		[Required]
		public Guid JobId { get; set; }

		public GetJobDetailsByIdRequest(Guid jobId)
		{
			JobId = jobId;
		}
	}
}