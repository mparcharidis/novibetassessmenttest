﻿using Heroic.AutoMapper;
using NovibetAssessmentTest.IpInfoProvider.Models;
using NovibetAssessmentTest.WebApi.Models;

namespace NovibetAssessmentTest.WebApi.Dtos
{
	public class IpDetailsDto : IMapFrom<IpDetails>, IMapFrom<IPDetails>
	{
		public string Ip { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string Continent { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
	}
}