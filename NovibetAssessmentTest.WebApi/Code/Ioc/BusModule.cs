﻿using System.Reflection;
using Autofac;
using MediatR;
using NovibetAssessmentTest.WebApi.Code.Bus;
using Module = Autofac.Module;

namespace NovibetAssessmentTest.WebApi.Code.Ioc
{
	public class BusModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			var assembliesToScan = new[]
			{
				Assembly.GetAssembly(typeof(IAmBackendAssembly)),
			};

			var mediatrOpenTypes = new[]
			{
				typeof(IRequestHandler<,>),
				typeof(INotificationHandler<>),
			};

			builder
				.RegisterType<Mediator>()
				.As<IMediator>()
				.SingleInstance();

			builder
				.Register<ServiceFactory>(context =>
				{
					var ctx = context.Resolve<IComponentContext>();
					return t => ctx.Resolve(t);
				});

			foreach (var mediatrOpenType in mediatrOpenTypes)
			{
				builder
					.RegisterAssemblyTypes(assembliesToScan)
					.AsClosedTypesOf(mediatrOpenType)
					.AsImplementedInterfaces()
					.PropertiesAutowired();
			}
		}
	}
}