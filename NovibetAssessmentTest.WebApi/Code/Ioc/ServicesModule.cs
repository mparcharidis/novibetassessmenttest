﻿using Autofac;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using NovibetAssessmentTest.WebApi.Code.Caching;
using NovibetAssessmentTest.WebApi.Services;

namespace NovibetAssessmentTest.WebApi.Code.Ioc
{
	public class ServicesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder
				.RegisterType<CacheProvider>()
				.As<ICacheProvider>()
				.InstancePerDependency()
				.PropertiesAutowired();
			
			builder
				.RegisterType<CacheService>()
				.As<ICacheService>()
				.InstancePerDependency()
				.PropertiesAutowired();
			
			builder
				.RegisterType<MemoryCache>()
				.As<IMemoryCache>()
				.SingleInstance()
				.PropertiesAutowired();

			builder
				.RegisterType<IpInfoReadService>()
				.As<IIpInfoReadService>()
				.SingleInstance()
				.PropertiesAutowired();
		}
	}
}