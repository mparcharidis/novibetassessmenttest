﻿using Autofac;
using NovibetAssessmentTest.WebApi.Repositories;

namespace NovibetAssessmentTest.WebApi.Code.Ioc
{
	public class RepositoriesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder
				.RegisterType<IpInfoReadRepository>()
				.As<IIpInfoReadRepository>()
				.InstancePerDependency()
				.PropertiesAutowired();
			
			builder
				.RegisterType<IpInfoWriteRepository>()
				.As<IIpInfoWriteRepository>()
				.InstancePerDependency()
				.PropertiesAutowired();
		}
	}
}