﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NovibetAssessmentTest.WebApi.Code.Validation
{
	public abstract class ValidateMe
	{
		private readonly object _objectToValidate;

		protected ValidateMe()
		{
			_objectToValidate = this;
		}
		
		private IEnumerable<string> GetValidationErrors()
		{
			IList<ValidationResult> results = new List<ValidationResult>();
			TryValidateObject(_objectToValidate, results);
			return results.Select(v => v.ErrorMessage);
		}
		
		private static bool TryValidateObject(object obj, ICollection<ValidationResult> results)
		{
			return Validator.TryValidateObject(obj, new ValidationContext(obj, null, null), results, true);
		}
		
		public void Validate()
		{
			IEnumerable<string> validationErrors = GetValidationErrors().ToList();
			if(!validationErrors.Any()) return;

			var msg = string.Join(",", validationErrors.ToArray());
			
			throw new Exception(msg);
		}
	}
}