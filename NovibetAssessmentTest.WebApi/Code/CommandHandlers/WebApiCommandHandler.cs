﻿using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using MediatR;
using NovibetAssessmentTest.WebApi.Code.Bus;
using NovibetAssessmentTest.WebApi.Dtos.Commands;
using NovibetAssessmentTest.WebApi.Repositories;

namespace NovibetAssessmentTest.WebApi.Code.CommandHandlers
{
	public class WebApiCommandHandler : IBusEnabled,
		IHandler<IpDetailsBatchUpdateCommand>
	{
		private readonly IIpInfoWriteRepository _ipInfoWriteRepository;
		
		public WebApiCommandHandler(IIpInfoWriteRepository ipInfoWriteRepository)
		{
			_ipInfoWriteRepository = ipInfoWriteRepository;
		}
		
		public IMediator Mediator { get; set; }
		
		public async Task<Unit> Handle(IpDetailsBatchUpdateCommand command, CancellationToken cancellationToken)
		{
			command.Validate();
			
				var hangFireInternalJobId = BackgroundJob.Enqueue(() => _ipInfoWriteRepository.BatchUpdateIpDetails(command.IpDetailsDtoList, command.CorrelationId));

				return Unit.Value;
		}
	}
}