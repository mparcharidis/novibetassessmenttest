﻿using System;

namespace NovibetAssessmentTest.WebApi.Code.Bus
{
	public interface IMessage
	{
		Guid MessageId { get; set; }
		Guid CorrelationId { get; set; }
	}
}