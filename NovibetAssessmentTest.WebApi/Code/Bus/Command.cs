﻿using System;
using AutoMapper;
using MediatR;
using NovibetAssessmentTest.WebApi.Code.Validation;

namespace NovibetAssessmentTest.WebApi.Code.Bus
{
	public abstract class Command : ValidateMe, IMessage, IRequest
	{
		protected Command()
		{
			MessageId = Guid.NewGuid();
			CorrelationId = Guid.NewGuid();
		}
		
		[IgnoreMap]
		public Guid MessageId { get; set; }
		
		[IgnoreMap]
		public Guid CorrelationId { get; set; }
	}
}