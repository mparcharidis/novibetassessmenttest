﻿using System;
using AutoMapper;
using MediatR;

namespace NovibetAssessmentTest.WebApi.Code.Bus
{
	public class Event : INotification, IMessage
	{
		protected Event(Guid correlationId)
		{
			CorrelationId = correlationId;
			MessageId = Guid.NewGuid();
		}
		
		[IgnoreMap]
		public Guid MessageId { get; set; }

		public Guid CorrelationId { get; set; }
	}
}