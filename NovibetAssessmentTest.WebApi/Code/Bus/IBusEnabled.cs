﻿using MediatR;

namespace NovibetAssessmentTest.WebApi.Code.Bus
{
	public interface IBusEnabled
	{
		IMediator Mediator { get; set; }
	}
}