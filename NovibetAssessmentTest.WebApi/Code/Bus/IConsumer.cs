﻿using MediatR;

namespace NovibetAssessmentTest.WebApi.Code.Bus
{
	public interface IConsumer<in TEvent> : INotificationHandler<TEvent> where TEvent : Event
	{
	}
}