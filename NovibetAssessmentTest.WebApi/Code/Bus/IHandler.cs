﻿using MediatR;

namespace NovibetAssessmentTest.WebApi.Code.Bus
{
	public interface IHandler<in TCommand> : IRequestHandler<TCommand> where TCommand : IRequest<Unit>
	{ 
	}
}