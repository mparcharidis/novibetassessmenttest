﻿using System;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace NovibetAssessmentTest.WebApi.Code.Caching
{
	public class CacheProvider : ICacheProvider
	{
		private readonly IMemoryCache _cache;
		private readonly int _cacheExpiringSeconds;

		public CacheProvider(IMemoryCache cache, IConfiguration configuration)
		{
			_cache = cache;
			_cacheExpiringSeconds = configuration.GetValue<int>("ApiSettings:CacheExpiringInSeconds");
		}
        
		public T GetFromCache<T>(string key) where T : class
		{
			_cache.TryGetValue(key, out T cachedResponse);
			return cachedResponse as T;
		}

		public void SetCache<T>(string key, T value) where T : class
		{
			SetCache(key, value, DateTimeOffset.Now.AddSeconds(_cacheExpiringSeconds));
		}

		public void SetCache<T>(string key, T value, DateTimeOffset duration) where T : class
		{
			_cache.Set(key, value, duration);
		}

		public void SetCache<T>(string key, T value, MemoryCacheEntryOptions options) where T : class
		{
			_cache.Set(key, value, options);
		}

		public void ClearCache(string key)
		{
			_cache.Remove(key);
		}
	}
}