﻿using Autofac;
using Heroic.AutoMapper;
using NovibetAssessmentTest.WebApi.Code.Ioc;

namespace NovibetAssessmentTest.WebApi.Code
{
	public class Bootstrapper
	{
		public static void SetupMapper()
		{
			HeroicAutoMapperConfigurator
				.LoadMapsFromAssemblyContainingTypeAndReferencedAssemblies<Bootstrapper>();
		}
		
		public static void RegisterModules(ContainerBuilder builder)
		{
			builder.RegisterModule<RepositoriesModule>();
			builder.RegisterModule<ServicesModule>();
			builder.RegisterModule<BusModule>();
		}
	}
}