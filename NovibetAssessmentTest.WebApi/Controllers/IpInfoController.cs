﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NovibetAssessmentTest.WebApi.Dtos;
using NovibetAssessmentTest.WebApi.Dtos.Commands;
using NovibetAssessmentTest.WebApi.Dtos.RequestResponse;
using NovibetAssessmentTest.WebApi.Services;

namespace NovibetAssessmentTest.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class IpInfoController : WebApiControllerBase
	{
		private readonly IIpInfoReadService _ipInfoReadService;
		private readonly IMediator _mediator;
		
		public IpInfoController(IIpInfoReadService ipInfoReadService, IMediator mediator)
		{
			_ipInfoReadService = ipInfoReadService;
			_mediator = mediator;
		}

		[HttpGet("{ip}")]
		public async Task<ActionResult<IpDetailsDto>> GetIpDetails(string ip)
		{
			try
			{
				GetIpDetailsByIpRequest request = new GetIpDetailsByIpRequest(ip);
				GetIpDetailsByIpResponse response = await _ipInfoReadService.GetIpDetailsByIpAsync(request);
				
				return Ok(response.IpDetailsDto);
			}
			catch (Exception ex)
			{
				Console.WriteLine($"The Ip service was not available and returned the following error(s): {ex.Message}");
				return StatusCode((int)HttpStatusCode.InternalServerError);
			}
		}

		[HttpPost]
		public async Task<ActionResult<Guid>> IpDetailsBatchUpdate(List<IpDetailsDto> ipDetailsDtoList)
		{
			IpDetailsBatchUpdateCommand command = new IpDetailsBatchUpdateCommand
			{
				IpDetailsDtoList = ipDetailsDtoList
			};

			await _mediator.Send(command);
			
			return Accepted(command.CorrelationId);
		}

		[HttpGet("job/{jobId}")]
		public async Task<ActionResult<string>> GetProgress(Guid jobId)
		{
			try
			{
				var request = new GetJobDetailsByIdRequest(jobId);
				var response = await _ipInfoReadService.GetJobDetailsByIpAsync(request);
				
				return Ok($"{response.BatchUpdateDetailsDto.CurrentIpsCounter}/{response.BatchUpdateDetailsDto.TotalIps}");
			}
			catch (Exception ex)
			{
				return ex is KeyNotFoundException ? NotFound() : StatusCode((int)HttpStatusCode.InternalServerError);
			}
		}
	}
}