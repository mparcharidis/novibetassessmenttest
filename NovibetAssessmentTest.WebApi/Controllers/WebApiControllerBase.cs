﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NovibetAssessmentTest.WebApi.Code.Bus;

namespace NovibetAssessmentTest.WebApi.Controllers
{
	[ApiController]
	public abstract class WebApiControllerBase : ControllerBase, IBusEnabled
	{
		public IMediator Mediator { get; set; }
	}
}