﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NovibetAssessmentTest.IpInfoProvider;
using NovibetAssessmentTest.IpInfoProvider.Models;
using NovibetAssessmentTest.WebApi.Dtos;
using NovibetAssessmentTest.WebApi.Models;

namespace NovibetAssessmentTest.WebApi.Repositories
{
	public class IpInfoReadRepository : IIpInfoReadRepository
	{
		private readonly IIPInfoProvider _ipInfoProvider;
		private readonly WebApiContext _dbContext;
		
		public IpInfoReadRepository(WebApiContext dbContext)
		{
			_dbContext = dbContext;
			_ipInfoProvider = new IPInfoProvider();
		}
		
		public async Task<IpDetailsDto> GetIpDetailsFromProviderByIdAsync(string ip)
		{
			IPDetails providerIpDetails = await _ipInfoProvider.GetDetails(ip);

			var ipDetailsDto = Mapper.Map<IpDetailsDto>(providerIpDetails);
			ipDetailsDto.Ip = ip;

			return ipDetailsDto;
		}
		
		public async Task<IpDetailsDto> GetIpDetailsFromDatabaseByIpAsync(string ip)
		{
			var ipDetailsFromDb = await _dbContext.IpDetails
				.Where(i => i.Ip == ip)
				.SingleOrDefaultAsync();

			return ipDetailsFromDb == null ? null : Mapper.Map<IpDetailsDto>(ipDetailsFromDb);
		}

		public async Task CreateIpDetailsAsync(IpDetailsDto ipDetailsDto)
		{
			var ipDetails = Mapper.Map<IpDetails>(ipDetailsDto);

			await _dbContext.IpDetails.AddAsync(ipDetails);
			await _dbContext.SaveChangesAsync();
		}

		public async Task<BatchUpdateDetailsDto> GetBatchUpdateDetailsAsync(Guid jobId)
		{
			var batchUpdateDetailsFromDb = await _dbContext.BatchUpdateDetails
				.FindAsync(jobId);

			return batchUpdateDetailsFromDb == null ? null : Mapper.Map<BatchUpdateDetailsDto>(batchUpdateDetailsFromDb);
		}
	}
}