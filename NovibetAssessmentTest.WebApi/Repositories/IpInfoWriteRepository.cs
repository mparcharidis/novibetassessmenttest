﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NovibetAssessmentTest.WebApi.Dtos;
using NovibetAssessmentTest.WebApi.Models;
using NovibetAssessmentTest.WebApi.Services;

namespace NovibetAssessmentTest.WebApi.Repositories
{
	public class IpInfoWriteRepository : IIpInfoWriteRepository
	{
		private readonly WebApiContext _dbContext;
		private readonly IConfiguration _configuration;
		private readonly ICacheService _cacheService;

		public IpInfoWriteRepository(WebApiContext dbContext, IConfiguration configuration, ICacheService cacheService)
		{
			_dbContext = dbContext;
			_configuration = configuration;
			_cacheService = cacheService;
		}

		private async Task CreateBatchDetailsAsync(BatchUpdateDetails batchUpdateDetails)
		{
			await _dbContext.BatchUpdateDetails.AddAsync(batchUpdateDetails);
			await _dbContext.SaveChangesAsync();
		}
		
		public async Task BatchUpdateIpDetails(List<IpDetailsDto> ipDetailsDtoList, Guid jobId)
		{
			var batchUpdateDetails = new BatchUpdateDetails
			{
				JobId = jobId,
				TotalIps = ipDetailsDtoList.Count,
				CurrentIpsCounter = 0
			};
			
			await CreateBatchDetailsAsync(batchUpdateDetails);

			int threshold = _configuration.GetValue<int>("ApiSettings:BatchThreshold");
			int counter = 0;
			var ipDetailsTempListForCaching = new List<IpDetailsDto>();
			
			foreach (var ipDetailsDto in ipDetailsDtoList)
			{
				var ipDetailsFetchResult = await _dbContext.IpDetails
					.SingleOrDefaultAsync(i => i.Ip == ipDetailsDto.Ip);
				
				if (ipDetailsFetchResult != null)
				{
					ipDetailsFetchResult.City = ipDetailsDto.City;
					ipDetailsFetchResult.Country = ipDetailsDto.Country;
					ipDetailsFetchResult.Continent = ipDetailsDto.Continent;
					ipDetailsFetchResult.Latitude = ipDetailsDto.Latitude;
					ipDetailsFetchResult.Longitude = ipDetailsDto.Longitude;
					
					ipDetailsTempListForCaching.Add(Mapper.Map<IpDetailsDto>(ipDetailsFetchResult));
					counter++;
				}
				else
				{
					var ipDetailsForCreation = Mapper.Map<IpDetails>(ipDetailsDto);
					await _dbContext.IpDetails.AddAsync(ipDetailsForCreation);
					ipDetailsTempListForCaching.Add(ipDetailsDto);
					
					counter++;
				}

				if (counter >= threshold || ipDetailsDtoList.Last() == ipDetailsDto)
				{
					batchUpdateDetails.CurrentIpsCounter += counter; 
					
					await _dbContext.SaveChangesAsync();
					await _cacheService.CreateCachedIpDetails(ipDetailsTempListForCaching);
					
					ipDetailsTempListForCaching.Clear();
					counter = 0;
				}
			}
		}

		
	}
}