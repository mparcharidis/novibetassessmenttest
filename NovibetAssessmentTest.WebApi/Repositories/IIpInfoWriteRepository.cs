﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NovibetAssessmentTest.WebApi.Dtos;

namespace NovibetAssessmentTest.WebApi.Repositories
{
	public interface IIpInfoWriteRepository
	{
		Task BatchUpdateIpDetails(List<IpDetailsDto> ipDetailsDtoList, Guid jobId);
	}
}