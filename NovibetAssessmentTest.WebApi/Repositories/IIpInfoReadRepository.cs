﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NovibetAssessmentTest.IpInfoProvider.Models;
using NovibetAssessmentTest.WebApi.Dtos;
using NovibetAssessmentTest.WebApi.Models;

namespace NovibetAssessmentTest.WebApi.Repositories
{
	public interface IIpInfoReadRepository
	{
		Task<IpDetailsDto> GetIpDetailsFromProviderByIdAsync(string ip);
		Task<IpDetailsDto> GetIpDetailsFromDatabaseByIpAsync(string ip);
		Task CreateIpDetailsAsync(IpDetailsDto ipDetailsDto);
		Task<BatchUpdateDetailsDto> GetBatchUpdateDetailsAsync(Guid jobId);
	}
}