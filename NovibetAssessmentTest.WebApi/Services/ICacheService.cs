﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NovibetAssessmentTest.WebApi.Dtos;

namespace NovibetAssessmentTest.WebApi.Services
{
	public interface ICacheService
	{
		Task<IpDetailsDto> GetCachedIpDetailsAsync(string ip);
		void ClearCache(string ip);
		Task CreateCachedIpDetails(IpDetailsDto ipDetailsDto);
		Task CreateCachedIpDetails(List<IpDetailsDto> ipDetailsDtoList);
	}
}