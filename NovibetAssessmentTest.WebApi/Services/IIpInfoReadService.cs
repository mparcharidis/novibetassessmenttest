﻿using System.Threading.Tasks;
using NovibetAssessmentTest.WebApi.Dtos.RequestResponse;

namespace NovibetAssessmentTest.WebApi.Services
{
	public interface IIpInfoReadService
	{
		Task<GetIpDetailsByIpResponse> GetIpDetailsByIpAsync(GetIpDetailsByIpRequest request);
		Task<GetJobDetailsByIpResponse> GetJobDetailsByIpAsync(GetJobDetailsByIdRequest request);
	}
}