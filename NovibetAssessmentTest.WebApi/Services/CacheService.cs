﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NovibetAssessmentTest.WebApi.Code.Caching;
using NovibetAssessmentTest.WebApi.Dtos;

namespace NovibetAssessmentTest.WebApi.Services
{
	public class CacheService : ICacheService
	{
		private readonly ICacheProvider _cacheProvider;

		public CacheService(ICacheProvider cacheProvider)
		{
			_cacheProvider = cacheProvider;
		}
		
		public async Task<IpDetailsDto> GetCachedIpDetailsAsync(string ip)
		{
			var ipDetailsDto = new IpDetailsDto();
			
			await Task.Run(() =>
			{
				ipDetailsDto = _cacheProvider.GetFromCache<IpDetailsDto>(ip);
			});

			return ipDetailsDto;
		}

		public async Task CreateCachedIpDetails(IpDetailsDto ipDetailsDto)
		{
			await Task.Run(() =>
			{
				_cacheProvider.SetCache(ipDetailsDto.Ip, ipDetailsDto);
			});
		}

		public async Task CreateCachedIpDetails(List<IpDetailsDto> ipDetailsDtoList)
		{
			await Task.Run(() =>
			{
				foreach (var ipDetailsDto in ipDetailsDtoList)
				{
					_cacheProvider.SetCache(ipDetailsDto.Ip, ipDetailsDto);
				}
			});
		}

		public void ClearCache(string ip)
		{
			_cacheProvider.ClearCache(ip);
		}
	}
}