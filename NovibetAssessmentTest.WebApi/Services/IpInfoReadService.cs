﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NovibetAssessmentTest.IpInfoProvider.Exceptions;
using NovibetAssessmentTest.WebApi.Dtos;
using NovibetAssessmentTest.WebApi.Dtos.RequestResponse;
using NovibetAssessmentTest.WebApi.Repositories;

namespace NovibetAssessmentTest.WebApi.Services
{
	public class IpInfoReadService : IIpInfoReadService
	{
		private readonly ICacheService _cacheService;
		private readonly IIpInfoReadRepository _ipInfoReadRepository;

		public IpInfoReadService(ICacheService cacheService, IIpInfoReadRepository ipInfoReadRepository)
		{
			_cacheService = cacheService;
			_ipInfoReadRepository = ipInfoReadRepository;
		}

		public async Task<GetIpDetailsByIpResponse> GetIpDetailsByIpAsync(GetIpDetailsByIpRequest request)
		{
			request.Validate();
			try
			{
				var ipDetailsDto = new IpDetailsDto();
				
				// checking cache for the specific record
				var cachedIpDetails = await _cacheService.GetCachedIpDetailsAsync(request.Ip);

				if (cachedIpDetails != null)
				{
					ipDetailsDto = cachedIpDetails;
					await _cacheService.CreateCachedIpDetails(ipDetailsDto);

					return new GetIpDetailsByIpResponse(ipDetailsDto);
				}
				
				// check if the record exists in database
				var ipDetailsFromDb = await _ipInfoReadRepository.GetIpDetailsFromDatabaseByIpAsync(request.Ip);  
				
				if (ipDetailsFromDb != null)
				{
					ipDetailsDto = ipDetailsFromDb;
					await _cacheService.CreateCachedIpDetails(ipDetailsDto);

					return new GetIpDetailsByIpResponse(ipDetailsDto);
				}
				
				// Fetching data from provider
				var ipDetailsFromProvider = await _ipInfoReadRepository.GetIpDetailsFromProviderByIdAsync(request.Ip);
				ipDetailsDto = ipDetailsFromProvider;
				ipDetailsDto.Ip = request.Ip;
				
				await _ipInfoReadRepository.CreateIpDetailsAsync(ipDetailsDto);
				await _cacheService.CreateCachedIpDetails(ipDetailsDto);

				return new GetIpDetailsByIpResponse(ipDetailsDto);

			}
			catch (Exception ex)
			{
				if(ex is IPServiceNotAvailableException)
					throw new IPServiceNotAvailableException(ex.Message, ex);

				string msg = $"Error occured while trying to get Ip details of Ip: {request.Ip}";
				throw new Exception(msg, ex);
			}
		}

		public async Task<GetJobDetailsByIpResponse> GetJobDetailsByIpAsync(GetJobDetailsByIdRequest request)
		{
			request.Validate();
			try
			{
				var batchUpdateDetails = await _ipInfoReadRepository.GetBatchUpdateDetailsAsync(request.JobId);
				
				if (batchUpdateDetails == null)
				{
					throw new KeyNotFoundException($"The Job with Job Id {request.JobId} was not found");
				}
				
				var response = new GetJobDetailsByIpResponse(batchUpdateDetails);
				return response;
			}
			catch (Exception ex)
			{
				if(ex is KeyNotFoundException)
					throw new KeyNotFoundException(ex.Message, ex);

				string msg = $"Error occured while trying to get the Job with Id: {request.JobId}";
				throw new Exception(msg, ex);
			}
			
		}
	}
}