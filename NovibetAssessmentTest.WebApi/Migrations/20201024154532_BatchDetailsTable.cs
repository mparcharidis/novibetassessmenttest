﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NovibetAssessmentTest.WebApi.Migrations
{
    public partial class BatchDetailsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BatchUpdateDetails",
                columns: table => new
                {
                    JobId = table.Column<Guid>(nullable: false),
                    TotalIps = table.Column<int>(nullable: false),
                    CurrentIpsCounter = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchUpdateDetails", x => x.JobId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BatchUpdateDetails");
        }
    }
}
