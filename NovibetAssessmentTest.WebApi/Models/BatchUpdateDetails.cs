﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NovibetAssessmentTest.WebApi.Models
{
	public class BatchUpdateDetails
	{
		[Key]
		public Guid JobId { get; set; }

		public int TotalIps { get; set; }
		public int CurrentIpsCounter { get; set; }
	}
}