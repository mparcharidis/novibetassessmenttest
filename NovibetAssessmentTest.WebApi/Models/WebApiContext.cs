﻿using Microsoft.EntityFrameworkCore;

namespace NovibetAssessmentTest.WebApi.Models
{
	public class WebApiContext : DbContext
	{
		public WebApiContext(DbContextOptions<WebApiContext> options) : base(options)
		{
		}
		
		public DbSet<IpDetails> IpDetails { get; set; }
		public DbSet<BatchUpdateDetails> BatchUpdateDetails { get; set; }
	}
}