﻿using System;
using System.ComponentModel.DataAnnotations;
using Heroic.AutoMapper;
using NovibetAssessmentTest.WebApi.Dtos;

namespace NovibetAssessmentTest.WebApi.Models
{
	public class IpDetails : IMapFrom<IpDetailsDto>
	{
		[Key]
		public Guid Id { get; set; }
		public string Ip { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string Continent { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
	}
}