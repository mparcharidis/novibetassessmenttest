﻿## Novibet Assessment Test - Assumptions ##

### General Assumptions ###

* We assume that the solution has proper unit testing

* We assume that there is a logging system which tracking all system changes and keeps a log for the messaging system we are using (MediatR)

* We assume that the backend jobs workflow includes policies for retrying, cancellation and more